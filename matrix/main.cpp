#include <iostream>
#include <utility>
#include <random>
#include <chrono>

#include "../common/stop_watch.hpp"

static const size_t kN = 1000;

int a[kN][kN], b[kN][kN], c[kN][kN];

void MultiplyNaive() {
  for (size_t i = 0; i < kN; ++i) {
    for (size_t j = 0; j < kN; ++j) {
      c[i][j] = 0;
      for (size_t k = 0; k < kN; ++k) {
        c[i][j] += a[i][k] * b[k][j];
      }
    }
  }
}

void MultiplyCacheAware() {
  // Transpose B
  for (size_t i = 0; i < kN; ++i) {
    for (size_t j = i + 1; j < kN; ++j) {
      std::swap(b[i][j], b[j][i]);
    }
  }
  // Multiply
  for (size_t i = 0; i < kN; ++i) {
    for (size_t j = 0; j < kN; ++j) {
      c[i][j] = 0;
      for (size_t k = 0; k < kN; ++k) {
        c[i][j] += a[i][k] * b[j][k];
      }
    }
  }
}

void GenerateInput() {
  std::mt19937 twister{42};

  for (size_t i = 0; i < kN; ++i) {
    for (size_t j = 0; j < kN; ++j) {
      a[i][j] = twister();
      b[i][j] = twister();
    }
  }
}

size_t ComputeOutputDigest() {
  size_t digest = 0;
  for (size_t i = 0; i < kN; ++i) {
    for (size_t j = 0; j < kN; ++j) {
      digest = c[i][j] ^ (digest << 1);
    }
  }
  return digest;
}

int main() {
  GenerateInput();

  auto start = std::chrono::steady_clock::now();

  MultiplyNaive();

  auto elapsed = std::chrono::steady_clock::now() - start;

  std::cout << "Output digest = " << ComputeOutputDigest() << std::endl;

  std::cout << "Elapsed: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count()
    << "ms" << std::endl;

  return 0;
}