#pragma once

#include <chrono>

class StopWatch {
  using Clock = std::chrono::steady_clock;
 public:
  StopWatch() {
    start_ = Clock::now();
  }

  size_t ElapsedMillis() const {
    return ToMillis(Elapsed());
  }

 private:
  Clock::duration Elapsed() const {
    return Clock::now() - start_;
  }

  static size_t ToMillis(Clock::duration d) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(d).count();
  }

 private:
  Clock::time_point start_;
};
